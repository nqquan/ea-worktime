//
//  Employee.swift
//  Evolable Asia Worktime
//
//  Created by Nguyen Quan on 2/23/16.
//  Copyright © 2016 Nguyen Quan. All rights reserved.
//

import UIKit

class Employee: NSObject, NSCoding {

    // MARK: Types
    struct PropertyKey {
        static let nameKey = "name"
        static let emailKey = "email"
        static let passwordKey = "password"
        static let isLateKey = "isLate"
    }

    // MARK: Properties
    var name: String
    var email: String
    var password: String
    var isLate : Bool

    // MARK: Archiving Paths
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("employees")

    // MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: PropertyKey.nameKey)
        aCoder.encodeObject(email, forKey: PropertyKey.emailKey)
        aCoder.encodeObject(password, forKey: PropertyKey.passwordKey)
        aCoder.encodeObject(isLate, forKey: PropertyKey.isLateKey)
    }

    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey(PropertyKey.nameKey) as? String ?? ""
        let email = aDecoder.decodeObjectForKey(PropertyKey.emailKey) as? String ?? ""
        let password = aDecoder.decodeObjectForKey(PropertyKey.passwordKey) as? String ?? ""
        let isLate = aDecoder.decodeObjectForKey(PropertyKey.isLateKey) as? Bool ?? false
        self.init(name: name, email: email, password: password, isLate: isLate)
    }

    // MARK: Initialization
    init?(name: String, email: String, password: String, isLate: Bool) {
        
        // Initialize stored properties.
        self.name = name
        self.email = email
        self.password = password
        self.isLate = isLate
        super.init()
    }

}
