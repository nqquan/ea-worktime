//
//  StartViewController.swift
//  Worktime
//
//  Created by Nguyen Quan on 2/24/16.
//  Copyright © 2016 Nguyen Quan. All rights reserved.
//

import UIKit
import Alamofire

class StartViewController: UIViewController {
    
    // MARK : properties
    @IBOutlet weak var reasonTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reasonTextView.layer.borderColor = UIColor(colorLiteralRed: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).CGColor
        reasonTextView.layer.borderWidth = 1.0
        reasonTextView.layer.cornerRadius = 5.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK: - Action
    @IBAction func send() {
        Alamofire.request(.POST, ViewController.startUrl, parameters:[
            "_method" : "POST",
            "data[ewt_users][reasonlate]" : self.reasonTextView.text,
            "data[ewt_users][started]" : 1,
            ])
            .responseString { response in
                var title : String, message: String?
                if response.result.isSuccess {
                    title = NSLocalizedString("message_login_success", value: "Login successfully.", comment: "message_login_success")
                    message = nil
                } else {
                    title = NSLocalizedString("message_can_not_start", value: "Oops! Error on start logging worktime.", comment: "message_can_not_start")
                    message = NSLocalizedString("message_try_login_on_web", value: "Please try loggin on website. Sorry about it.", comment: "message_try_login_on_web")
                }
                let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
                let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                alert.addAction(OKAction)
                self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
