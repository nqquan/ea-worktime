//
//  EndViewController.swift
//  Evolable Asia Worktime
//
//  Created by Nguyen Quan on 2/25/16.
//  Copyright © 2016 Nguyen Quan. All rights reserved.
//

import UIKit
import Alamofire
import Fuzi
import MBProgressHUD

class EndViewController: UIViewController {

    static let punchOutUrl = "https://worktime.evolable.asia/ewt_users/view"
    static let domain = "https://worktime.evolable.asia"
    static let xpathPunchOutUrl = "//*[@id=\"EwtUserAddForm\"]"
    
    // MARK: - Properties
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var reasonTextView: UITextView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var punchoutButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        reasonTextView.layer.borderColor = UIColor(colorLiteralRed: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).CGColor
        reasonTextView.layer.borderWidth = 1.0
        reasonTextView.layer.cornerRadius = 5.0

        updateDisplayTime()
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateDisplayTime", userInfo: nil, repeats: true)
        
        if let employee: Employee = NSKeyedUnarchiver.unarchiveObjectWithFile(Employee.ArchiveURL.path!) as? Employee {
            self.navigationItem.title = employee.name
            if employee.isLate {
                // come late -> show input reason view
                if let startViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StartNavigationController") {
                    self.presentViewController(startViewController, animated: true, completion: {
                        print("Login sucessfull")
                    })
                }
            }
        }

        self.noteTextView.text = NSLocalizedString("time_different_note", comment: "")
    }

    // MARK: - Navigation
    @IBAction func logout(sender: UIBarButtonItem) {
        let cookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookie : NSHTTPCookie in cookieStorage.cookies! {
            cookieStorage.deleteCookie(cookie)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func hideKeyboard(sender: UITapGestureRecognizer) {
        reasonTextView.resignFirstResponder()
    }
    
    @IBAction func punchOut() {

        let hub = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hub.labelText = NSLocalizedString("message_logging_out", comment: "Punching out")
        hub.show(true)

        Alamofire.request(.GET, EndViewController.punchOutUrl)
            .responseString(completionHandler: { response in
                if let url = response.response?.URL?.URLString {
                    switch (url) {
                    case ViewController.loginUrl:
                        hub.hide(true)
                        let title = NSLocalizedString("message_session_timeout", value: "Oops! Login session timeout.", comment: "Oops! Login session timeout.")
                        let message = NSLocalizedString("message_login_again", value: "Please login again.", comment: "Please login again.")
                        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
                        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        alert.addAction(OKAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                        break

                    case ViewController.viewUrl:
                        
                        if let doc = try? HTMLDocument(string: response.result.value!, encoding: NSUTF8StringEncoding) {
                            if let elements : XPathNodeSet = doc.xpath(EndViewController.xpathPunchOutUrl) where elements.count > 0  {
                                
                                let url = elements.first?.attr("action")
                                Alamofire.request(.POST, EndViewController.domain + url!, parameters: [
                                    "_method" : "POST",
                                    "data[EwtUser][reason]" : self.reasonTextView.text
                                    ])
                                    .responseString(completionHandler: { response in
                                        hub.hide(true)

                                        let title = NSLocalizedString("message_punchout_success", value: "Yeah! Punchout successfully.", comment: "Yeah! Punchout successfully.")
                                        let alert = UIAlertController(title: title, message: nil, preferredStyle: .Alert)
                                        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                                            alert.dismissViewControllerAnimated(true, completion: nil)
                                        }
                                        alert.addAction(OKAction)
                                        self.presentViewController(alert, animated: true, completion: nil)
                                    })
                            }
                        }
                        break

                    default:

                        break
                    }
                }

                if let doc = try? HTMLDocument(string: response.result.value!, encoding: NSUTF8StringEncoding) {
                    if let elements : XPathNodeSet = doc.xpath(EndViewController.xpathPunchOutUrl) where elements.count > 0  {

                        let url = elements.first?.attr("action")
                        Alamofire.request(.POST, EndViewController.domain + url!, parameters: [
                            "_method" : "POST",
                            "data[EwtUser][reason]" : self.reasonTextView.text
                            ])
                            .responseString(completionHandler: { response in
                                if (response.result.isSuccess) {
                                    
                                } else {
                                }
                            })
                    }
                }
            })
    }
    

    func updateDisplayTime() {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second], fromDate: NSDate())
        self.timerLabel.text = "Local time: \(components.hour):\(components.minute):\(components.second)";

        self.punchoutButton.enabled = reasonTextView.text.characters.count >= 2 || components.hour >= 17
    }

}
