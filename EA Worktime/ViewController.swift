//
//  ViewController.swift
//  Evolable Asia Worktime
//
//  Created by Nguyen Quan on 2/22/16.
//  Copyright © 2016 Nguyen Quan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Fuzi
import MBProgressHUD

class ViewController: UIViewController {
    
    // MARK: static
    static let captchaUrl: String = "https://worktime.evolable.asia/ewt_users/captcha_image/"
    static let loginUrl: String = "https://worktime.evolable.asia/ewt_users/login"
    static let listLogTimeUrl = "https://worktime.evolable.asia/ewt_attendance_records/listlogofuser";
    static let viewUrl = "https://worktime.evolable.asia/ewt_users/view";
    static let startUrl = "https://worktime.evolable.asia/ewt_users/start";
    
    static var xpathWrongPassword: String = "//*[@id=\"error\"]/text()"
    static var xpathWrongCaptcha: String = "//*[@id=\"username_row\"]/td[2]/span[@class=\"err-message\"]/text()"
    static var xpathWelcomeText = "//*[@class=\"wel\"]/a/text()"
    
    // MARK: Properties
    @IBOutlet weak var captchaImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var captchaTextField: UITextField!
    var employee : Employee?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let hub = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hub.labelText = NSLocalizedString("checking_connection", comment: "Checking connection")
        hub.show(true)
        
        Alamofire.request(.GET, ViewController.captchaUrl)
            .responseImage(completionHandler: { response in

                hub.hide(true)
                if response.response != nil {
                    switch response.response!.statusCode {
                    case 200, 201, 202:
                        if let image = response.result.value {
                            print("Image downloaded: \(image)")
                            self.captchaImageView.image = image
                        } else {
                            print("Logged in already!")
                            
                        }
                    case 404:
                        self.outsiteOfficeAlert()
                        break
                    default:
                        break
                    }
                } else {
                    self.noNetworkAlert()
                }

            })

        if let employee: Employee = NSKeyedUnarchiver.unarchiveObjectWithFile(Employee.ArchiveURL.path!) as? Employee {
            self.employee = employee
            self.emailTextField.text = employee.email
            self.passwordTextField.text = employee.password
        } else {
            self.employee = Employee(name: "", email: "", password: "", isLate: false)!
        }
    }
    
    // MARK: - NSCoding
    func saveInformation() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(self.employee!, toFile: Employee.ArchiveURL.path!)
        if !isSuccessfulSave {
            print("Failed to save employee ")
        }
    }
    
    // MARK: Event
    @IBAction func emailChange() {
        self.employee!.email = self.emailTextField.text!
        saveInformation()
    }
    
    @IBAction func passwordChange() {
        self.employee!.password = self.passwordTextField.text!
        saveInformation()
    }
    
    @IBAction func reloadCaptcha() {
        let hub = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hub.labelText = NSLocalizedString("checking_connection", comment: "Checking connection")
        hub.show(true)

        Alamofire.request(.GET, ViewController.captchaUrl + "?time=" + String(NSDate.timeIntervalSinceReferenceDate()))
            .responseImage(completionHandler: { response in
                hub.hide(true)
                if response.response != nil {
                    switch response.response!.statusCode {
                    case 200, 201, 202:
                        if let image = response.result.value {
                            print("Image downloaded: \(image)")
                            self.captchaImageView.image = image
                        } else {
                            print("Logged in already!")
                            
                        }
                    case 403:
                        self.outsiteOfficeAlert()
                        break
                    default:
                        break
                    }
                } else {
                    self.noNetworkAlert()
                }
            })
    }
    
    @IBAction func inputed(sender: UITextField) {
        switch sender {
        case self.emailTextField:
            passwordTextField.becomeFirstResponder()
            break
        case self.passwordTextField:
            captchaTextField.becomeFirstResponder()
            break
        case self.captchaTextField:
            self.captchaTextField.resignFirstResponder()
            break
        default:
            self.captchaTextField.resignFirstResponder()
            break
        }
    }
    
    @IBAction func hideKeyboard(sender: UITapGestureRecognizer) {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        self.captchaTextField.resignFirstResponder()
    }
    
    @IBAction func login() {
        
        Alamofire.request(.POST, ViewController.loginUrl, parameters: [
            "_method": "POST",
            "data[EwtUser][email_login]": emailTextField.text!,
            "data[EwtUser][password_login]": passwordTextField.text!,
            "data[EwtUser][captcha]": captchaTextField.text!])
            .responseString { response in
                
                if let doc = try? HTMLDocument(string: response.result.value!, encoding: NSUTF8StringEncoding) {

                    if let elements : XPathNodeSet = doc.xpath(ViewController.xpathWrongCaptcha) where elements.count > 0  {
                        // wrong captcha
                        let title = NSLocalizedString("message_login_failed", value: "Oops! Login problem.", comment: "Oops! Login problem.")
                        let alert = UIAlertController(title: title, message: elements.first?.rawXML.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), preferredStyle: .Alert)
                        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        alert.addAction(OKAction)
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else if let elements : XPathNodeSet = doc.xpath(ViewController.xpathWrongPassword) where elements.count > 0 {
                        // wrong email/password
                        let title = NSLocalizedString("message_login_failed", value: "Oops! Login problem.", comment: "Oops! Login problem.")
                        let alert = UIAlertController(title: title, message: elements.first?.rawXML.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), preferredStyle: .Alert)
                        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        alert.addAction(OKAction)
                        self.presentViewController(alert, animated: true, completion: nil)

                    } else if let elements : XPathNodeSet = doc.xpath(ViewController.xpathWelcomeText) where elements.count > 0 {
                        // login success
                        if let redirectedUrl = response.response?.URL?.URLString {
                            
                            // check for is late or not
                            self.employee!.isLate = (ViewController.startUrl == redirectedUrl)
                            self.saveInformation()
                            if  let endNavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("EndNavigationController") {
                                self.employee!.name = elements.first!.rawXML
                                self.saveInformation()
                                self.presentViewController(endNavigationController, animated: true, completion: nil)
                            }
                        }
                    }
                }

                self.captchaTextField.text = ""
                self.captchaImageView.image = nil
        }
    }
    
    // MARK: functions
    func outsiteOfficeAlert() {
        let title = NSLocalizedString("message_cannot_connect", value: "Oops! Unable to connect.", comment: "Oops! Unable to connect.")
        let message = NSLocalizedString("message_connect_office_network", value: "You must use the Evolable Asia network to login.", comment: "You must use the Evolable Asia network to login.")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(OKAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func noNetworkAlert() {
        let title = NSLocalizedString("message_no_network", value: "Oops! No network connected.", comment: "Oops! Unable to connect.")
        let message = NSLocalizedString("message_connect_office_network", value: "You must use the Evolable Asia network to login.", comment: "You must use the Evolable Asia network to login.")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(OKAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
}